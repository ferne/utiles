#!/bin/bash

# Versión 1. Sin nombre de usuario. Nombre de máquina "hardcoded".
# echo "** Stella Ubuntu *******************************************************"
# date +%Y-%m-%d%t%H:%M
# printf "    \n"

# Versión 2. Nombre de máquina con uname
# printf "\n** %s *******************************************************\n" `uname -n`
# date +%Y-%m-%d%t%H:%M
# printf "    \n"

# Versión 3. Nombre de usuario y nombre de máquina
# printf "\n#** %s ***************************************** %s ***\n#   " `uname -n` `id -u -n`
# date +%Y-%m-%d
# printf "#   "
# date +%H:%M

# Versión 4. Toda la información en una sola línea
usuario=`id -u -n`
host=`uname -n`
fecha=`date +%Y-%m-%d`
hora=`date +%H:%M`
printf "\n#** %s@%s ************************ %s ** %s ***\n    " $usuario $host $fecha $hora
