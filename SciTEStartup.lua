--     Este fichero se ha de guardar en la carpeta del usuario para que SciTe
-- lo cargue al inicio.

function InicioComentario(lenguaje)
--  se podría mejorar devolviendo no un valor, sino dos
--  el inicio del comentario y el final, con lo que se evitaría una
--  función.
  return props["comment.block."..lenguaje]
end

function CrearRelleno(longitud)
-- la longitud de la cadena devuelta será la introducida por el parámetro
-- más dos veces la longitud del espacio que en principio será de uno
  local caracterRelleno = "*"
  local espacio = " "
  local relleno = ""
  relleno = espacio..string.rep(caracterRelleno, longitud)..espacio
  return relleno
end

function ObtenerPlataforma()
    local plataforma = "DESCONOCIDO"
    local usuario = os.getenv("USERNAME")
    if (usuario ~= nil) then
        plataforma = "WINDOWS"
        return plataforma
    end
    usuario = os.getenv("USER")
    if (usuario ~= nil) then
        if os.getenv("HOSTNAME") ~= nil then
            plataforma = "OSX"
        else
            plataforma = "LINUX"
        end
        return plataforma
    end
    return(plataforma)
end

-- Una forma alternativa de averiguar el entorno en el que nos encontramos.
function ObtenerClavesEntorno__(tabla)
    plataforma = ObtenerPlataforma()
    if plataforma == "WINDOWS" then
        tabla.user = os.getenv("USERNAME")
        tabla.host = os.getenv("COMPUTERNAME")
        return tabla
    end
    if plataforma == "LINUX" then
        tabla.user = os.getenv("USER")
        tabla.host = io.popen("uname -n"):read()
        return tabla
    end
    if plataforma == "OSX" then
        tabla.user = os.getenv("USER")
        tabla.host = os.getenv("HOSTNAME")
        return tabla
    end
    return tabla
end

function ObtenerClavesEntorno(tabla)
--    plataforma = ObtenerPlataforma()
    if props['PLAT_WIN'] == "1" then
        tabla.user = os.getenv("USERNAME")
        tabla.host = os.getenv("COMPUTERNAME")
        return tabla
    end
    if props['PLAT_GTK'] == "1" then
        tabla.user = os.getenv("USER")
        tabla.host = io.popen("uname -n"):read()
        return tabla
    end
    if props['PLAT_MAC'] == "1" then
        tabla.user = os.getenv("USER")
        tabla.host = os.getenv("HOSTNAME")
        return tabla
    end
    return tabla
end

function CrearSeparador()
-- Insertar una línea de separación que incluye el nombre de la máquina en la
-- que se editó el texto a la parte izquierda de la línea y la fecha y hora en la
-- parte derecha.
-- 2017.12.27 por dieferne en gmail punto com
-- 2020-11-16 Adaptado  para Linux.

  local fecha = os.date("%Y-%m-%d")
  local hora = os.date("%H:%M")
  local lenguaje = props['Language']
  local longitudLinea = props['edge.column']-4
  local inicioComentario = InicioComentario(lenguaje)
  local finComentario = ""
  tabla = {}
  tabla = ObtenerClavesEntorno(tabla)
  local inicioLinea = inicioComentario..CrearRelleno(3)
                            ..tabla.user.."@"..tabla.host
  finLinea = fecha
                 ..CrearRelleno(2)
                 ..hora
                 ..CrearRelleno(3)
  linea = "\n"
            ..inicioLinea
            ..CrearRelleno(longitudLinea
                                 - string.len(inicioLinea)
                                 - string.len(finLinea))
            ..finLinea
            .."\n    \n"
  editor:AddText(linea)
  nLinea = editor:LineFromPosition(editor.CurrentPos)
  nLinea = nLinea - 1
  editor:GotoLine(nLinea)
  editor:GotoPos(editor.CurrentPos + 4)
end



-- Tags used by os.date:
--   %a abbreviated weekday name (e.g., Wed)
--   %A full weekday name (e.g., Wednesday)
--   %b abbreviated month name (e.g., Sep)
--   %B full month name (e.g., September)
--   %c date and time (e.g., 09/16/98 23:48:10)
--   %d day of the month (16) [01-31]
--   %H hour, using a 24-hour clock (23) [00-23]
--   %I hour, using a 12-hour clock (11) [01-12]
--   %M minute (48) [00-59]
--   %m month (09) [01-12]
--   %p either "am" or "pm" (pm)
--   %S second (10) [00-61]
--   %w weekday (3) [0-6 = Sunday-Saturday]
--   %x date (e.g., 09/16/98)
--   %X time (e.g., 23:48:10)
--   %Y full year (1998)
--   %y two-digit year (98) [00-99]
--   %% the character '%'
