# Útiles

Pequeños programas útiles para algo...

## separador.sh
Comando interno para GEdit que crea una cabecera de separación compuesta de:
1. Una línea de asteriscos con el nombre de __usuario__ y de __máquina__ embebidos en ella.
2. La __fecha__ y __hora__.
3. Una línea con 4 espacios en blanco como tabulación de primera línea (el separador está pensado para encabezar un texto y utilizo el estándar español de empezar un párrafo por una tabulación).

## SciTEStarup.lua
Script en lua que crea una cabecera de separación compuesta de:
1. Una línea de asteriscos con el usuario y nombre de máquina en la parte de la izquierda y fecha y hora en la parte derecha.
3. Una línea con 4 espacios en blanco como tabulación de primera línea (el separador está pensado para encabezar un texto y utilizo el estándar español de empezar un párrafo por una tabulación).
